/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
/**
 *
 * @author Administrator
 */
public class TableUnit {
    
    public TableUnit() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    @Test
    public void testSwitchTurn(){
        Player x = new Player('x');
        Player o = new Player('o');
        Table table = new Table(x, o);
        if(table.getCurrentPlayer().getName()== 'x'){
            table.switchTurn();
            assertEquals('o', table.getCurrentPlayer().getName());
        }
        else if(table.getCurrentPlayer().getName() == 'o'){
            table.switchTurn();
            assertEquals('x',table.getCurrentPlayer().getName());
        }
    }
    
    @Test
    public void testRow() throws Exception{
        Player x =  new Player('x');
        Player o = new Player('o');
        Table table = new Table(x, o);
        table.setRowCol(1, 1);
        table.setRowCol(1, 2);
        table.setRowCol(1, 3);
        assertEquals(true, table.checkRow());
    }
    
    @Test
    public void testCol() throws Exception{
        Player x = new Player('x');
        Player o =  new Player('o');
        Table table = new Table(x, o);
        table.setRowCol(1, 1);
        table.setRowCol(2, 1);
        table.setRowCol(3, 1);
        assertEquals(true, table.checkCol());
    }
    
    @Test
    public void testCross1() throws Exception{
        Player x = new Player('x');
        Player o =  new Player('o');
        Table table = new Table(x, o);
        table.setRowCol(1, 1);
        table.setRowCol(2, 2);
        table.setRowCol(3, 3);
        assertEquals(true, table.checkCross1());
    }
    
     @Test
    public void testCross2() throws Exception{
        Player x = new Player('x');
        Player o =  new Player('o');
        Table table = new Table(x, o);
        table.setRowCol(1, 3);
        table.setRowCol(2, 2);
        table.setRowCol(3, 1);
        assertEquals(true, table.checkCross2());
    }
    
      @Test
    public void testDraw() throws Exception{
        Player x = new Player('x');
        Player o =  new Player('o');
        Table table = new Table(x, o);
        table.setRowCol(1, 1);
        table.switchTurn();
        table.setRowCol(2, 2);
        table.switchTurn();
        table.setRowCol(3, 3);
        table.switchTurn();
        table.setRowCol(2, 1);
        table.switchTurn();
        table.setRowCol(1, 2);
        table.switchTurn();
        table.setRowCol(1, 3);
        table.switchTurn();
        table.setRowCol(3, 1);
        table.switchTurn();
        table.setRowCol(3, 2);
        table.switchTurn();
        table.setRowCol(2, 3);
        assertEquals(true, table.checkDraw());
    }
    @Test
     public void testWin() throws Exception{
        Player x = new Player('x');
        Player o =  new Player('o');
        Table table = new Table(x, o);
        table.setRowCol(1, 1);
        table.setRowCol(2, 2);
        table.setRowCol(3, 3);
        assertEquals(true, table.checkWin());
    }
}
